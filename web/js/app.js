var app = new Vue({
    el: '#app',
    data: {
        element : "",
        list: [
        ],
        totalList :  Number(0),
        budget :  Number(0),
        bgc : ''
    },
    methods: {
        addItem: function(){
            this.list.push({id: this.list.length, text: this.element, price: 0, check: false})
        },
        deleteItem: function(id){
            for (var i = 0; i < this.list.length; i++) {
                
                if (this.list[i].id == id) {
                    this.list.splice(i,1);
                    this.editPrice();
                    this.editBudget();
                    break;
                }
            } 
        },
        editPrice: function(){
            this.totalList =  Number(0)
            for (var i = 0; i < this.list.length; i++) {
                if (this.list[i].check){
                    this.totalList +=  Number(this.list[i].price) 
                }
            } 
            this.editBudget();
        },
        toggleChecked: function(id){
            for (var i = 0; i < this.list.length; i++) {
                if (this.list[i].id == id) {
                    this.list[i].check = ! this.list[i].check
                    this.editPrice();
                    break;
                }
            } 
        },
        isChecked: function(id){
            for (var i = 0; i < this.list.length; i++) {
                if (this.list[i].id == id) {
                    return this.list[i].check
                }
            } 
        },
        editBudget: function(){
            if (this.budget >= this.totalList){
                this.bgc = 'border-color:green'
            }else{
                this.bgc = 'border-color:red'
            }
        }
        
    }
    
})